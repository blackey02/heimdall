﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace HeimdallServer
{
    class Program
    {
        private static Listener _listener = new Listener();

        static void Main(string[] args)
        {
            SetConsoleCtrlHandler(new HandlerRoutine(CloseHandler), true);
            _listener.Start();
        }

        private static bool CloseHandler(CtrlTypes type)
        {
            _listener.Dispose();
            return true;
        }

        #region unmanaged
        [DllImport("Kernel32")]
        public static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);

        public delegate bool HandlerRoutine(CtrlTypes CtrlType);

        public enum CtrlTypes
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT
        }
        #endregion
    }
}
