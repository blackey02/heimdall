﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using HeimdallUtils.Logging;
using System.Threading;
using HeimdallUtils;

namespace HeimdallServer
{
    class Listener : IDisposable
    {
        TcpListener _server = null;

        public void Start()
        {
            try
            {
                int port = Properties.Settings.Default.Port;
                var localAddress = IPAddress.Parse("127.0.0.1");
                _server = new TcpListener(localAddress, port);

                // Start listening for client requests.
                _server.Start();

                // Enter the listening loop. 
                while (true)
                {
                    Logger.Instance.Log("Waiting for a connection...");

                    // Perform a blocking call to accept requests. 
                    // You could also user server.AcceptSocket() here.
                    TcpClient client = _server.AcceptTcpClient();
                    Logger.Instance.Log(string.Format("Connected to {0}", client.Client.RemoteEndPoint));

                    // Hand the client off to be handled
                    ThreadPool.QueueUserWorkItem(obj => Handle(client));
                }
            }
            catch (SocketException e)
            {
                Logger.Instance.Log("SocketException: {0}", e);
            }
            finally
            {
                // Stop listening for new clients.
                _server.Stop();
            }
        }

        private void Handle(TcpClient client)
        {
            // Buffer for reading data
            var buffer = new byte[1024];
            var bufferQueue = new Queue<byte[]>();

            // Get a stream object for reading and writing
            NetworkStream stream = client.GetStream();

            // Loop to receive all the data sent by the client.
            do
            {
                int bytesRead = stream.Read(buffer, 0, buffer.Length);
                var tempBuffer = new byte[bytesRead];
                Array.Copy(buffer, 0, tempBuffer, 0, bytesRead);
                bufferQueue.Enqueue(tempBuffer);
            }
            while (stream.DataAvailable);

            buffer = bufferQueue.SelectMany(a => a).ToArray();
            byte[] decryptedData = Crypto.Instance.Decrypt<byte[]>(Properties.Settings.Default.Password, buffer);

            string outMessage = string.Empty;
            if (decryptedData != null)
            {
                Message message = Message.Deserialize(decryptedData);
                Logger.Instance.Log("Successfully decrypted message sent from {0} {1}", new object[] { client.Client.RemoteEndPoint, message.CallingUser },  Logger.LogLocation.File);
                outMessage = "Success";
            }
            else
            {
                Logger.Instance.Log("Failed to decrypt message sent from {0}", client.Client.RemoteEndPoint, Logger.LogLocation.File);
                outMessage = "Error";
            }

            // Send reply back to the user
            byte[] outBuffer = Encoding.UTF8.GetBytes(outMessage);
            outBuffer = Crypto.Instance.Encrypt(Properties.Settings.Default.Password, outBuffer);
            stream.Write(outBuffer, 0, outBuffer.Length);

            // Shutdown and end connection
            stream.Close();
            client.Close();
        }

        #region IDisposable Members
        public void Dispose()
        {
            Logger.Instance.Dispose();
        }
        #endregion
    }
}
