﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace HeimdallUtils
{
    public class Crypto
    {
        private static Crypto _instance = null;

        public static Crypto Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Crypto();
                return _instance;
            }
        }

        // This constant is used to determine the keysize of the encryption algorithm.
        private const int KeySize = 256;

        // This constant string is used as a "salt" value for the PasswordDeriveBytes function calls.
        // This size of the IV (in bytes) must = (KeySize / 8).  Default KeySize is 256, so the IV must be
        // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
        private static readonly byte[] Salt = Encoding.ASCII.GetBytes("tu89gheimdall9u2");

        private Crypto()
        { }

        public byte[] Encrypt<T>(string passPhrase, T data)
        {
            byte[] encryptedData = null;
            if (typeof(T) == typeof(string))
                encryptedData = EncryptData(passPhrase, (string)(object)data);
            else if(typeof(T) == typeof(byte[]))
                encryptedData = EncryptData(passPhrase, (byte[])(object)data);
            return encryptedData;
        }

        public T Decrypt<T>(string passPhrase, byte[] data)
        {
            T decryptedData = default(T);
            if (typeof(T) == typeof(string))
                decryptedData = (T)(object)DecryptToString(passPhrase, data);
            else if (typeof(T) == typeof(byte[]))
                decryptedData = (T)(object)DecryptData(passPhrase, data);
            return decryptedData;
        }

        private byte[] DecryptData(string passPhrase, byte[] data)
        {
            byte[] decryptedData = null;
            try
            {
                using (var password = new PasswordDeriveBytes(passPhrase, null))
                {
                    byte[] keyBytes = password.GetBytes(KeySize / 8);
                    using (var symmetricKey = new RijndaelManaged())
                    {
                        symmetricKey.Mode = CipherMode.CBC;
                        using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, Salt))
                        {
                            using (MemoryStream memoryStream = new MemoryStream(data))
                            {
                                using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                                {
                                    var tempDecryptedData = new byte[data.Length];
                                    int decryptedByteCount = cryptoStream.Read(tempDecryptedData, 0, tempDecryptedData.Length);
                                    decryptedData = new byte[decryptedByteCount];
                                    Array.Copy(tempDecryptedData, decryptedData, decryptedByteCount);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logging.Logger.Instance.Log(e.Message, Logging.Logger.LogLocation.File);
                decryptedData = null;
            }
            return decryptedData;
        }

        private string DecryptToString(string passPhrase, byte[] data)
        {
            byte[] decryptedData = DecryptData(passPhrase, data);
            return decryptedData == null ? string.Empty : Encoding.UTF8.GetString(decryptedData);
        }

        private byte[] EncryptData(string passPhrase, byte[] data)
        {
            byte[] encryptedData = null;
            try
            {
                using (var password = new PasswordDeriveBytes(passPhrase, null))
                {
                    byte[] keyBytes = password.GetBytes(KeySize / 8);
                    using (var symmetricKey = new RijndaelManaged())
                    {
                        symmetricKey.Mode = CipherMode.CBC;
                        using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, Salt))
                        {
                            using (var memoryStream = new MemoryStream())
                            {
                                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                                {
                                    cryptoStream.Write(data, 0, data.Length);
                                    cryptoStream.FlushFinalBlock();
                                    encryptedData = memoryStream.ToArray();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logging.Logger.Instance.Log(e.Message, Logging.Logger.LogLocation.File);
                encryptedData = null;
            }
            return encryptedData;
        }

        private byte[] EncryptData(string passPhrase, string data)
        {
            byte[] plainTextData = Encoding.UTF8.GetBytes(data);
            return Encrypt(passPhrase, plainTextData);
        }
    }
}
