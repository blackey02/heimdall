﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace HeimdallUtils.Logging
{
    class LogSystem : LogBase 
    {
        private EventLog _eventLog;

        public LogSystem()
        {
            _eventLog = new EventLog(string.Empty);
            _eventLog.Source = "Heimdall";
        }

        protected override void LogData(string data)
        {
            _eventLog.WriteEntry(data, EventLogEntryType.FailureAudit);
        }

        protected override void Dispose(bool disposing)
        {
            _eventLog.Dispose();
            base.Dispose(disposing);
        }
    }
}
