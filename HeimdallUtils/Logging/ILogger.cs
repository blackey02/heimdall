﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeimdallUtils.Logging
{
    interface ILogger : IDisposable
    {
        void Log(object data);
        void Log(string format, object data);
        void Log(string format, object[] data);
    }
}
