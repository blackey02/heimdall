﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeimdallUtils.Logging
{
    abstract class LogBase : ILogger
    {
        protected abstract void LogData(string data);

        #region ILogger Members
        public void Log(object data)
        {
            var dataStr = string.Format("{0} - {1}", DateTime.Now, data);
            LogData(dataStr);
        }

        public void Log(string format, object data)
        {
            string formattedData = string.Format(format, data);
            Log(formattedData);
        }

        public void Log(string format, object[] data)
        {
            string formattedData = string.Format(format, data);
            Log(formattedData);
        }
        #endregion

        protected virtual void Dispose(bool disposing)
        { }

        #region IDisposable Members
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
