﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace HeimdallUtils.Logging
{
    class LogFile : LogBase
    {
        private StreamWriter _logWriter;

        public LogFile()
        {
            string exeDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string exeLogPath = Path.Combine(exeDir, "log.txt");
            _logWriter = File.Exists(exeLogPath) ? File.AppendText(exeLogPath) : File.CreateText(exeLogPath);
        }

        protected override void LogData(string data)
        {
            _logWriter.WriteLine(data);
        }

        protected override void Dispose(bool disposing)
        {
            _logWriter.Flush();
            _logWriter.Dispose();
            base.Dispose(disposing);
        }
    }
}
