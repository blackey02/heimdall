﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeimdallUtils.Logging
{
    public class Logger : IDisposable
    {
        private static Logger _instance = null;

        public static Logger Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Logger();
                return _instance;
            }
        }

        private Dictionary<LogLocation, ILogger> _loggers = new Dictionary<LogLocation, ILogger>();

        private Logger()
        {
            InitLoggers();
        }

        [Flags]
        public enum LogLocation { System = 0x1, Debug = 0x2, File = 0x4 }

        private void InitLoggers()
        {
            _loggers.Add(LogLocation.Debug, new LogDebug());
            _loggers.Add(LogLocation.File, new LogFile());
            _loggers.Add(LogLocation.System, new LogSystem());
        }

        public void Log(object data, LogLocation location = LogLocation.Debug)
        {
            if ((location & LogLocation.Debug) == LogLocation.Debug)
                _loggers[LogLocation.Debug].Log(data);
            if ((location & LogLocation.File) == LogLocation.File)
                _loggers[LogLocation.File].Log(data);
            if ((location & LogLocation.System) == LogLocation.System)
                _loggers[LogLocation.System].Log(data);
        }

        public void Log(string format, object data, LogLocation location = LogLocation.Debug)
        {
            string formattedData = string.Format(format, data);
            Log(formattedData, location);
        }

        public void Log(string format, object[] data, LogLocation location = LogLocation.Debug)
        {
            string formattedData = string.Format(format, data);
            Log(formattedData, location);
        }

        #region IDisposable Members
        public void Dispose()
        {
            foreach (LogLocation location in Enum.GetValues(typeof(LogLocation)))
                _loggers[location].Dispose();
            _instance = null;
        }
        #endregion
    }
}
