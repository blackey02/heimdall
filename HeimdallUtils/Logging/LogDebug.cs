﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace HeimdallUtils.Logging
{
    class LogDebug : LogBase
    {
        protected override void LogData(string data)
        {
            Debug.WriteLine(data);
        }
    }
}
