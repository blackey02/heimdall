﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace HeimdallUtils
{
    [Serializable]
    public class Message
    {
        public string CallingUser { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public string Password { get; set; }

        public static byte[] Serialize(Message message)
        {
            byte[] serialized;
            var formatter = new BinaryFormatter();
            using (var stream = new MemoryStream())
            {
                formatter.Serialize(stream, message);
                serialized = stream.ToArray();
            }
            return serialized;
        }

        public static Message Deserialize(byte[] buffer)
        {
            Message message = null;
            var formatter = new BinaryFormatter();
            using (var stream = new MemoryStream(buffer))
            {
                message = (Message)formatter.Deserialize(stream);
            }
            return message;
        }
    }
}
