﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;

namespace HeimdallClient
{
    public partial class HeimdallForm : Form
    {
        public HeimdallForm()
        {
            InitializeComponent();

            System.Threading.Thread.Sleep(5000);
            try
            {
                int port = Properties.Settings.Default.Port;
                var client = new TcpClient("127.0.0.1", port);
                var stream = client.GetStream();

                var msg = new HeimdallUtils.Message();
                msg.CallingUser = string.Format("{0}\\{1}\\{2}", Environment.UserDomainName, Environment.MachineName, Environment.UserName);
                msg.FullName = "brad lackey";
                msg.Description = "brads desc";
                msg.Password = "brads pw";
                msg.Username = "brads username";

                byte[] data = HeimdallUtils.Crypto.Instance.Encrypt("admin", HeimdallUtils.Message.Serialize(msg));

                stream.Write(data, 0, data.Length);

                var bufferQueue = new Queue<byte[]>();
                var buffer = new byte[1024];
                stream.BeginRead(buffer, 0, buffer.Length, result =>
                {
                    int bytesRead = stream.EndRead(result);
                    var tempBuffer = new byte[bytesRead];
                    Array.Copy(buffer, 0, tempBuffer, 0, bytesRead);
                    bufferQueue.Enqueue(tempBuffer);

                    while (stream.DataAvailable)
                    {
                        bytesRead = stream.Read(buffer, 0, buffer.Length);
                        tempBuffer = new byte[bytesRead];
                        Array.Copy(buffer, 0, tempBuffer, 0, bytesRead);
                        bufferQueue.Enqueue(tempBuffer);
                    }

                    buffer = bufferQueue.SelectMany(a => a).ToArray();
                    buffer = HeimdallUtils.Crypto.Instance.Decrypt<byte[]>("admin", buffer);
                    string reply = Encoding.UTF8.GetString(buffer);

                    stream.Close();
                    client.Close();

                    HeimdallUtils.Logging.Logger.Instance.Log(reply);
                }, null);
            }
            catch (Exception)
            { }
        }
    }
}
