Heimdall
====================

![](https://copy.com/RcBhATNFroA0) 

Heimdall is the all-seeing and all-hearing guardian sentry of Asgard who stands on the rainbow bridge Bifröst to watch for any attacks to Asgard. He partly won the role through using his eyesight to see an army of giants several days' march from Asgard, allowing them to be defeated before they reached Asgard, and making their king a prisoner. For ages, he stood as the guardian of Asgard, defending the city's gates from any intruders, and was one of the most trusted servants of Odin.

Summary
---------------------
Heimdall is a relatively simple client - server application that allows sending information from one application to another securely and effeciently.

Features
---------------------
1. Provides a good starting point for transferring simple data between two computers
2. Encryption
3. Extensive logging to console, file or/and the event log

Building
---------------------
1. `git clone https://bitbucket.org/blackey02/heimdall.git`
2. Open the Visual Studio solution file and build the application
3. Edit the app.settings file for the Heimdall server application to include your unique master password and port in which you want to use
4. Edit the app.settings file for the Heimdall client application to include the same port setting as the server
5. When programming the Heimdall client application make sure to encrypt the data with the same password that's set on the server or it will not be able to decrypt the message and will ultimately fail

Screens
---------------------
* ![](https://copy.com/Xc1EzINWqj9V)
* ![](https://copy.com/z92JEYg66hJ8)
* ![](https://copy.com/gRhz1qtMOnH0)
* ![](https://copy.com/UfzljBqPoOu6)